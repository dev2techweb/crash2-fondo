<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>"
	data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

<section id="conocenos">
</section>
<div class="negro">
<table class="margenes">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY1.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY2.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY3.png" alt=""></td>
    </tr>
  </thead>
</table>

<table class="margenes2">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY4.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY5.png" alt=""></td>
    </tr>
  </thead>
</table>
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/SEGUNDAPAGINA-04.png" alt="">
</div>








<div class="negro2">
<table class="margenes22">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY1.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY2.png" alt=""></td>
    </tr>
  </thead>
</table>

<table class="margenes22">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY3.png" alt=""></td>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY4.png" alt=""></td>
    </tr>
  </thead>
</table>

<table class="margenes222">
  <thead>
    <tr>
	  <td><img src="<?php echo get_template_directory_uri(); ?>/assets/images/DAY5.png" alt=""></td>
    </tr>
  </thead>
</table>

</div>





<section id="surf">
</section>

<section id="campamento">
</section>
